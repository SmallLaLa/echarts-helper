package indi.xulala.data;

import indi.xulala.style.TextStyle;

/**
 * @Author：xulei
 * @Description：
 * 图例的数据数组。
 * 数组项通常为一个字符串，每一项代表一个系列的 name（如果是饼图，也可以是饼图单个数据的 name）。
 * 图例组件会自动根据对应系列的图形标记（symbol）来绘制自己的颜色和标记，特殊字符串 ''（空字符串）或者 '\n'（换行字符串）用于图例的换行。
 * 如果 data 没有被指定，会自动从当前系列中获取。多数系列会取自 series.name 或者 series.encode 的 seriesName 所指定的维度。如 饼图 and 漏斗图 等会取自 series.data 中的 name。
 * 如果要设置单独一项的样式，也可以把该项写成配置项对象。此时必须使用 name 属性对应表示系列的 name。
 * 示例
 * Data: [{
 * name: '系列1',
 * // 强制设置图形为圆。
 * icon: 'circle',
 * // 设置文本为红色
 * textStyle: {
 * color: 'red'
 * }
 * }]
 * @Date：2018/4/3 0003 13:57
 */
public class LegendData {
    /**
     * 图例项的名称，应等于某系列的name值（如果是饼图，也可以是饼图单个数据的 name）。
     */
    private String name;
    /**
     * 图例项的 icon。
     * ECharts 提供的标记类型包括 'circle', 'rect', 'roundRect', 'triangle', 'diamond', 'pin', 'arrow'
     * 也可以通过 'image://url' 设置为图片，其中 url 为图片的链接，或者 dataURI。
     * 可以通过 'path://' 将图标设置为任意的矢量路径。这种方式相比于使用图片的方式，不用担心因为缩放而产生锯齿或模糊，而且可以设置为任意颜色。路径图形会自适应调整为合适的大小。路径的格式参见 SVG PathData。可以从 Adobe Illustrator 等工具编辑导出。
     */
    private String icon;
    /**
     * 图例项的文本样式。
     */
    private TextStyle textStyle;


    public LegendData() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public TextStyle getTextStyle() {
        return textStyle;
    }

    public void setTextStyle(TextStyle textStyle) {
        this.textStyle = textStyle;
    }
}