package indi.xulala.enums;

/**
 * @Author：xulei
 * @Description：
 * @Date：2018/4/3 0003 09:37
 */
public enum FontStyle {
    normal,italic,oblique;

    FontStyle() {
    }
}
